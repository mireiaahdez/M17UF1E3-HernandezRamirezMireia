using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fps : MonoBehaviour
{
    [SerializeField]
     private Text _fps;
    [SerializeField]
    private float _hudRateRefresh = 1f;
    private float timer;

    void Start()
    {
        
    }

    
    void Update()
    {
        if(Time.unscaledDeltaTime > timer)
        {
            int fps = (int)(1f / Time.unscaledDeltaTime);
            _fps.text = "FPS: " + fps;
            timer = Time.unscaledDeltaTime + _hudRateRefresh;
        }    
    }
}
