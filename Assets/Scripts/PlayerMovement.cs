using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public DataPlayer _dataPlayer;
    public Transform _transform;

    public Sprite[] sprites = new Sprite[3]; 
    public SpriteRenderer _spriteRenderer;
    int spriteIndex = 0;

    public bool isMoving;
    private bool changeDirection = true;
    public float distanceToX;
    public float distanceToY;
    
    void Start()
    {
        Application.targetFrameRate = 12;

        distanceToX = 0;
        distanceToY = 0;
        changeDirection = true;
         
        _dataPlayer = gameObject.GetComponent<DataPlayer>();
        _spriteRenderer.sprite = sprites[0];

        if(_dataPlayer.weight <= 15f)
        {
            _dataPlayer.speed = 0.6f;

        }else if(_dataPlayer.weight <= 30f) {

            _dataPlayer.speed = 0.3f;

        }else if(_dataPlayer.weight <= 45f)
        {
            _dataPlayer.speed = 0.1f;

        }else{

            _dataPlayer.speed = 0.02f;
        }


       
    }

    
    void Update()
    {

        if (changeDirection)
        {
            GoLeft();

        }
        else{

            GoRight();

        }

    }

    public void GoRight()
    {
        if(isMoving == true)
        {
            _spriteRenderer.sprite = sprites[spriteIndex];

            if(spriteIndex == 2)
            {
                spriteIndex = 1;

            }
            else
            {
                spriteIndex++;
            }

            if(distanceToY >= _dataPlayer.distance)
            {
                _spriteRenderer.flipX = false;
                _spriteRenderer.sprite = sprites[0];
                changeDirection = true;

                distanceToY = 0;

            }
            else
            {
                transform.position = new Vector3(transform.position.x + _dataPlayer.speed, transform.position.y);
                distanceToY += _dataPlayer.speed;

            }

        }
        else
        {
            _spriteRenderer.sprite= sprites[0];
        }

    }



    public void GoLeft()
    {

        if(isMoving==true)
        {

            _spriteRenderer.sprite = sprites[spriteIndex];

            if(spriteIndex == 2)
            {
                spriteIndex = 1;

            }
            else
            {
                spriteIndex++;
            }

            if(distanceToX >= _dataPlayer.distance)
            {
                _spriteRenderer.flipX = true;
                _spriteRenderer.sprite = sprites[0];
                changeDirection = false;
                distanceToX = 0;

            }
            else
            {

                transform.position = new Vector3(transform.position.x - _dataPlayer.speed, transform.position.y);
                distanceToX += _dataPlayer.speed;

            }

        }
        else
        {
            Debug.Log(_spriteRenderer.sprite);
            _spriteRenderer.sprite = sprites[0];
        }

    }
}
