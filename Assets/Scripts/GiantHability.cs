using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GiantHability : MonoBehaviour
{

    private enum StatusPlayer
    {
        idle,
        giant,
        grow,
        ungrow,
        cooldown
    }
    private StatusPlayer currentState;
    public float _coolDown = 8f;
    private float timeRemaining = 5f;


    public DataPlayer _dataPlayer;
    private Vector3 initScale;
    private Vector3 maximScale;

    bool scaling = false;
    bool ready = true;
    bool ungrowing = false;


    void Start()
    {
        _dataPlayer = GetComponentInParent<DataPlayer>();
        initScale = transform.localScale;
        maximScale = initScale + new Vector3(1, 1, 1) * (_dataPlayer.height / 10);
        currentState = StatusPlayer.idle;
    }


    void Update()
    {
        switch (currentState)
        {
            case StatusPlayer.idle:
                idle();
                break;

            case StatusPlayer.giant:
                giant();
                break;

            case StatusPlayer.grow:
                grow();
                break;

            case StatusPlayer.ungrow:
                ungrow();
                break;

            case StatusPlayer.cooldown:
                cooldown();
                break;
        }

    }


    void idle()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            currentState = StatusPlayer.grow;
        }
    }

    void giant()
    {
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
        }
        else
        {
            timeRemaining = 5f;
            currentState = StatusPlayer.ungrow;
        }
    }

    void grow()
    {
        if (ready)
        {
            ready = false;
            scaling = true;
        }

        if (scaling)
        {
            transform.localScale += new Vector3(1, 1, 1);
            if (transform.localScale.x >= maximScale.x)
            {
                scaling = false;
                currentState = StatusPlayer.giant;
            }
        }
    }

    void ungrow()
    {
        ungrowing = true;
        if (ungrowing)
        {
            transform.localScale -= new Vector3(1, 1, 1);
            if (transform.localScale.x <= initScale.x)
            {
                ungrowing = false;
                currentState = StatusPlayer.cooldown;
            }
        }

       
    }

    void cooldown()
    {
        if(_coolDown > 0)
        {
            _coolDown -= Time.deltaTime;
        }
        else
        {
            ready = true;
            _coolDown = 8f;
            currentState = StatusPlayer.idle;
        }
    }
}
