using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerRoles
{
    Knight,
    Warrior,
    Sorcerer,
    Mage
}

public class DataPlayer : MonoBehaviour
{
   
    

    public PlayerRoles _playerRoles;

    [SerializeField]
    public string name;
    public float height;
    public float weight;
    public float speed;
    public float distance;
    public int lives;

    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
